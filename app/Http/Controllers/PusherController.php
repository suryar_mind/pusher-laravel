<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pusher\Pusher;
use Pusher\PusherException;

class PusherController extends Controller
{
    /**
     * Send a new Pusher Broadcasting message through a channel
     *
     * @param Request $request
     * @return redirect
     */
    public function send(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'message' => 'required',
            'channel' => 'required'
        ]);

        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'useTLS' => true
        );

        try {
            $pusher = new Pusher(
                env('PUSHER_APP_KEY'),
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'),
                $options
            );
        } catch (PusherException $e) {
        }

        $data['title'] = $request->title;
        $data['message'] = $request->message;

        try {
            $pusher->trigger($request->channel, 'my-event', $data);
        } catch (PusherException $e) {
        }

        return redirect()->back();
    }
}
