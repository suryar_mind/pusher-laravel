
$(document).ready(function () {

    const titleBox = $('#titleBox');
    const messageBox = $('#messageBox');
    const submitBtn = $('#submitBtn');
    const responseDiv = $('#responseDiv');

    const getMessages = () => {
        fetch('http://127.0.0.1:8000/api/messages')
            .then((res) => res.json())
            .then((res) => render(res))
    };

    const setMessages = (form) => {
        $.post('http://127.0.0.1:8000/api/save', form)
            .then((res) => res.json())
            .then((res) => console.log('Saved Successfully'))
    };

    const render = (message) => {
        let html = ``;
        for(let i=0; i < message.records.length; i++) {
            html += `
                <div class="alert alert-success mb-3">
                    <strong>Title: ${message.records[i].id} - ${message.records[i].title}</strong>
                    <br>
                    <a>Message: ${message.records[i].message}</a>
                    <br>
                    <a>Time: ${message.records[i].created_at}</a>
                </div>
            `;
        }
        responseDiv.append(html);
    };

    submitBtn.on('click', function(event) {
        const form = {
            'title': titleBox.val(),
            'message': messageBox.val()
        };
        setMessages(form);
    });

    getMessages();
});
