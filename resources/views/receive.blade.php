<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{env('APP_NAME')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="{{asset('js/app.js')}}" type="application/javascript"></script>

        <style>
            .flow {
                overflow-wrap: break-word;
            }
        </style>
    </head>

    <body class="container jumbotron border border-dark mt-4 pt-3" style="min-height: 600px;">
    <h3>Receivers</h3>
    <hr>
    <br>
    <div class="row mx-4 my-auto text-center">
        <div class="flex-center position-ref col-lg-4 col-sm-12">
            <strong>Channel 1</strong>
            <hr>
            <div id="alertContainer1"></div>
        </div>

        <div class="flex-center position-ref col-lg-4 col-sm-12">
            <strong>Channel 2</strong>
            <hr>
            <div id="alertContainer2"></div>
        </div>

        <div class="flex-center position-ref col-lg-4 col-sm-12">
            <strong>Channel 3</strong>
            <hr>
            <div id="alertContainer3"></div>
        </div>

        <script src="{{asset('js/pusher.js')}}" type="application/javascript"></script>
    </div>
    </body>
</html>
