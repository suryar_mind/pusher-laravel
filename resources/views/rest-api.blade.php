<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{env('APP_NAME')}}</title>

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="{{asset('js/app.js')}}" type="application/javascript"></script>
        <script src="{{asset('js/rest.js')}}" type="application/javascript"></script>

    </head>
    <body class="container jumbotron jumbotron-fluid">
        <div class="col-8 text-center">

            <strong class="mt-8">Send a Rest Message</strong>
            <hr>
            <form id="formInput">
                @csrf

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Title</label>

                    <div class="col-md-6">
                        <input type="text" name="title" id="titleBox" class="form-control" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Message</label>

                    <div class="col-md-6">
                        <textarea name="message" id="messageBox" class="form-control" rows="5" required></textarea>
                    </div>
                </div>

                <div class="form-group row m-auto">
                    <label class="col-md-4 col-form-label text-md-right">Send</label>

                    <div class="col-md-6">
                        <button type="button" id="submitBtn" class="btn btn-danger btn-block">
                            Send
                        </button>
                    </div>
                </div>

            </form>

            <br>
            <hr>
            <div id="responseDiv"></div>
        </div>
    </body>
</html>
