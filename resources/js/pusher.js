
$(document).ready(function () {
    // Pusher.logToConsole = true;

    const pusher = new Pusher('d7720ecac6b4493c129d', {
        cluster: 'ap2'
    });

    let channel1 = pusher.subscribe('channel-demo1');
    let channel2 = pusher.subscribe('channel-demo2');
    let channel3 = pusher.subscribe('channel-demo3');

    channel1.bind('my-event', function(data) {
        $('#alertContainer1').append(message(data, 'primary'))
    });

    channel2.bind('my-event', function(data) {
        $('#alertContainer2').append(message(data, 'success'))
    });

    channel3.bind('my-event', function(data) {
        $('#alertContainer3').append(message(data, 'info'))
    });
});

function message(content, type = 'secondary') {
    return `<div class="alert alert-${type} font-weight-bold flow">
                <strong>Title: ${content['title']}</strong>
                <br>
                <a>Message: ${content['message']}</a>
            </div>`;
}