<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageApiController extends Controller
{
    public function save(Request $request)
    {
        $request->validate([
            'title' => ['required', 'max:20'],
            'message' => ['required', 'max:50']
        ]);

        Message::create([
            'title' => $request->title,
            'message' => $request->message,
        ]);

        $response = [
            'status' => 200,
            'message' => 'Record inserted Successfully'
        ];

        return response()->json($response);
    }

    public function retrieve()
    {
        $messages = Message::orderBy('id', 'desc')->take(5)->get();

        $response = [
            'status' => 200,
            'records' => $messages
        ];

        return response()->json($response);
    }
}
