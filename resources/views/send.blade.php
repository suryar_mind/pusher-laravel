<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{env('APP_NAME')}}</title>

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="{{asset('js/app.js')}}" type="application/javascript"></script>


    </head>
    <body class="container jumbotron jumbotron-fluid">
        <div class="col-12 text-center">

            <strong class="mt-8">Send a Broadcast message</strong>
            <hr>
            <form action="{{ route('send.submit') }}" method="POST">
                @csrf

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Title</label>

                    <div class="col-md-6">
                        <input type="text" name="title" class="form-control" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Message</label>

                    <div class="col-md-6">
                        <textarea name="message" class="form-control" rows="5" required></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Channel</label>

                    <div class="col-md-6">
                        <select name="channel" class="form-control" required>
                            <option value="">Select</option>
                            <option value="channel-demo1">Channel 1</option>
                            <option value="channel-demo2">Channel 2</option>
                            <option value="channel-demo3">Channel 3</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row m-auto">
                    <label class="col-md-4 col-form-label text-md-right">Send</label>

                    <div class="col-md-6">
                        <input type="submit" class="btn btn-danger btn-block">
                    </div>
                </div>

            </form>
        </div>
    </body>
</html>
