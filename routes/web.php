<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/send', function () {
    return view('send');
});

Route::post('/send', 'PusherController@send')->name('send.submit');

Route::get('/receive', function () {
    return view('receive');
});

Route::get('/rest', function () {
    return view('rest-api');
});
